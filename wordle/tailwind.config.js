/** @type {import('tailwindcss').Config} */
module.exports = {
  darkMode: "class",
  content: ["./src/**/*.{js,jsx,ts,tsx}"],
  theme: {
    extend: {
      backgroundColor: {
        wordle: "#2B3041",
        tools: "rgba(218, 220, 224, 0.03)",
        "keyboard-light": "rgba(218, 220, 224, 0.3)",
        "keys-dark": "#565F7E",
        "keys-light": "#D3D6DA",
        "modal-light": "rgba(243, 243, 243, 0.89)",
      },
      textColor: {
        "keys-light": "#56575E",
      },
      borderColor: {
        "modal-dark": "#939B9F",
      },
    },
  },
  plugins: [],
};
