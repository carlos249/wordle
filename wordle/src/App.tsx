import React from "react";
import "./App.css";
import Wordle from "./pages/Wordle";
import { SettingsProvider } from "./context/SettingsContext";

const App = () => {
  return (
    <AppState>
      <Wordle />
    </AppState>
  );
};

const AppState = ({ children }: any) => {
  return <SettingsProvider>{children}</SettingsProvider>;
};

export default App;
