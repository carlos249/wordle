import React, { useContext, useEffect } from "react";
import "./styles/modalStatistics.scss";
import { SettingsContext } from "../context/SettingsContext";
interface ModalStatisticsProps {
  hiddenModal: () => void;
  wordSelect: string;
  type: "won" | "lost";
  chronometer: any;
  isShowModal: boolean;
}
const ModalStatistics = ({
  hiddenModal,
  wordSelect,
  type,
  chronometer,
  isShowModal,
}: ModalStatisticsProps) => {
  const { settingsState } = useContext(SettingsContext);

  if (
    !chronometer.isRunning &&
    chronometer.minutes === 0 &&
    chronometer.seconds === 0
  ) {
    const time = new Date();
    time.setSeconds(time.getSeconds() + 300);
    chronometer.restart(time);
  }

  if (isShowModal) {
    return (
      <>
        <div className="justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none">
          <div className="relative w-auto my-6 mx-auto max-w-3xl text-black dark:text-white">
            {/*content*/}
            <div className="statistics border-black dark:border-modal-dark shadow-lg relative flex flex-col w-full bg-modal-light dark:bg-[#262B3C] outline-none focus:outline-none">
              {/*header*/}
              <div className="flex justify-center border-solid border-slate-200 rounded-t">
                <h3 className="statistics-title self-center mt-20">
                  Estadísticas
                </h3>
              </div>

              <div className="flex justify-around mt-10">
                <div className="flex flex-col items-center">
                  <h4 className="statistics-title">{settingsState.games}</h4>
                  <p className="statistics-games">Jugadas</p>
                </div>

                <div className="flex flex-col items-center">
                  <h4 className="statistics-title">{settingsState.win}</h4>
                  <p className="statistics-games">Victorias</p>
                </div>
              </div>

              {type === "lost" && (
                <p className="statistics-word text-center">
                  La palabra era: <span>{wordSelect}</span>
                </p>
              )}
              <p className="statistics-word  mt-7 text-center uppercase">
                SIGUIENTE PALABRA
              </p>
              <p className="statistics-timer text-center mt-2 mb-10">{`${chronometer.minutes}:${chronometer.seconds}`}</p>

              <button
                className="statistics-button bg-[#6AAA64] self-center active:bg-emerald-500 uppercase  shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150"
                type="button"
                onClick={hiddenModal}
              >
                Aceptar
              </button>
              {/*body*/}
            </div>
          </div>
        </div>
        <div className="opacity-25 fixed inset-0 z-40 bg-black"></div>
      </>
    );
  } else {
    return null;
  }
};

export default ModalStatistics;
