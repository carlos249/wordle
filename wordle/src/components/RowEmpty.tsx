import React from "react";
import Box from "./Box";
import styles from "./styles/row.module.scss";

export const RowEmpty = () => {
  return (
    <div className={styles.row}>
      {Array.from(Array(5)).map((_, index) => (
        <Box key={index} value="" status="empty" />
      ))}
    </div>
  );
};
