import React, { useContext } from "react";
import "./styles/headertools.scss";
import { ReactComponent as Instructions } from "../assets/icons/instructions.svg";
import { ReactComponent as InstructionsGray } from "../assets/icons/instructions-gray.svg";
import { ReactComponent as Statistics } from "../assets/icons/statistics.svg";
import { ReactComponent as StatisticsGray } from "../assets/icons/statistics-gray.svg";
import { ReactComponent as SwitchBodyLight } from "../assets/icons/switch-body-light.svg";
import { ReactComponent as SwitchCircleLight } from "../assets/icons/switch-circle-light.svg";
import { ReactComponent as SwitchBodyDark } from "../assets/icons/switch-body-dark.svg";
import { ReactComponent as SwitchCircleDark } from "../assets/icons/switch-circle-dark.svg";
import Switch from "react-switch";
import { SettingsContext } from "../context/SettingsContext";

interface HeaderToolsProps {
  showModalInstructions: () => void;
  showModalStatistics: () => void;
}

const HeaderTools = ({
  showModalInstructions,
  showModalStatistics,
}: HeaderToolsProps) => {
  const { settingsState, toggleTheme } = useContext(SettingsContext);

  return (
    <div className="tools dark:bg-tools ">
      {settingsState.theme === "dark" ? (
        <InstructionsGray
          className="tools-instructions"
          onClick={showModalInstructions}
        />
      ) : (
        <Instructions
          className="tools-instructions"
          onClick={showModalInstructions}
        />
      )}

      <h1 className="text-[#202537] dark:text-[#DADCE0]">WORDLE</h1>
      <div className="tools-show">
        {settingsState.theme === "dark" ? (
          <StatisticsGray
            onClick={showModalStatistics}
            className="tools-statistics"
          />
        ) : (
          <Statistics
            onClick={showModalStatistics}
            className="tools-statistics"
          />
        )}
        <Switch
          checkedIcon={<SwitchBodyLight />}
          uncheckedIcon={
            <SwitchBodyDark
              style={{
                position: "absolute",
                width: 53,
                height: 53,
                top: -13,
                left: -23,
              }}
            />
          }
          checkedHandleIcon={
            <SwitchCircleLight
              style={{
                position: "absolute",
                top: -6,
                left: -10,
                height: 43,
                width: 43,
              }}
            />
          }
          uncheckedHandleIcon={
            <SwitchCircleDark
              style={{
                position: "absolute",
                top: 0,
                left: 0,
                height: 28,
                width: 28,
              }}
            />
          }
          onChange={() =>
            toggleTheme(settingsState.theme === "dark" ? "light" : "dark")
          }
          checked={settingsState.theme === "light" ? true : false}
        />
      </div>
    </div>
  );
};

export default HeaderTools;
