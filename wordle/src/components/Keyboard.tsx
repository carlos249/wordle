import React, { useContext } from "react";
import "./styles/keyboard.scss";
import { ReactComponent as BackspaceDark } from "../assets/icons/backspace-dark.svg";
import { ReactComponent as BackspaceLight } from "../assets/icons/backspace-light.svg";
import { SettingsContext } from "../context/SettingsContext";

interface KeyboardProps {
  keys: string[];
  onKeyPressed: (key: string) => void;
  completeWords: string[];
  wordOfTheDay: string;
}

const Keyboard = ({
  keys,
  onKeyPressed,
  completeWords,
  wordOfTheDay,
}: KeyboardProps) => {
  const { settingsState } = useContext(SettingsContext);
  function handleInput(e: any) {
    onKeyPressed(e.target.textContent);
  }
  function handleEnter(e: any) {
    onKeyPressed("ENTER");
  }
  function handleDelete(e: any) {
    onKeyPressed("BACKSPACE");
  }
  function findLetter(letra: string): string {
    let color = "none";
    completeWords.map((palabra, index) => {
      if (palabra.includes(letra) && wordOfTheDay.includes(letra)) {
        for (let index2 = 0; index2 < palabra.length; index2++) {
          if (wordOfTheDay[index2] === letra) {
            color = "correct";
            return;
          } else {
            color = "present";
            return;
          }
        }
      } else if (palabra.includes(letra) && !wordOfTheDay.includes(letra)) {
        color = "absent";
        return;
      }
    });
    return color;
  }
  return (
    <div
      className={
        "keyboardContainer bg-keyboard-light dark:bg-tools dark:text-white text-keys-light"
      }
    >
      <div className="keyboardFirst flex">
        {Array.from(Array(10)).map((item, index) => {
          return (
            <button
              key={index}
              className={
                findLetter(keys[index]) === "correct"
                  ? "correct text-white"
                  : findLetter(keys[index]) === "present"
                  ? "present"
                  : findLetter(keys[index]) === "none"
                  ? "key bg-keys-light dark:bg-keys-dark"
                  : "absent text-white"
              }
              onClick={handleInput}
            >
              {keys[index]}
            </button>
          );
        })}
      </div>

      {/* <div className={"emptyKey"}></div> */}
      <div className="keyboardSecond flex">
        {Array.from(Array(10)).map((item, index) => (
          <button
            key={index + 10}
            className={
              findLetter(keys[index + 10]) === "correct"
                ? "correct text-white"
                : findLetter(keys[index + 10]) === "present"
                ? "present"
                : findLetter(keys[index + 10]) === "none"
                ? "key bg-keys-light dark:bg-keys-dark"
                : "absent text-white"
            }
            onClick={handleInput}
          >
            {keys[index + 10]}
          </button>
        ))}
      </div>
      <div className="keyboardThree flex">
        <button
          className={"enterKey bg-keys-light dark:bg-keys-dark "}
          onClick={handleEnter}
        >
          ENTER
        </button>
        {Array.from(Array(7)).map((item, index) => {
          return (
            <button
              key={index + 20}
              className={
                findLetter(keys[index + 20]) === "correct"
                  ? "correct text-white"
                  : findLetter(keys[index + 20]) === "present"
                  ? "present"
                  : findLetter(keys[index + 20]) === "none"
                  ? "key bg-keys-light dark:bg-keys-dark"
                  : "absent text-white"
              }
              onClick={handleInput}
            >
              {keys[index + 20]}
            </button>
          );
        })}
        <button
          className={"deleteKey bg-keys-light dark:bg-keys-dark"}
          onClick={handleDelete}
        >
          {settingsState.theme === "light" ? (
            <BackspaceLight />
          ) : (
            <BackspaceDark />
          )}
        </button>
      </div>
    </div>
  );
};

export default Keyboard;
