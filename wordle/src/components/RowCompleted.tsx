import React from "react";
import Box from "./Box";
import styles from "./styles/row.module.scss";
import { BoxStatus } from "../configs/types";
import { removeAccents } from "../services/request";
interface RowCompletedProps {
  word: string;
  solution: string;
}
const RowCompleted = ({ word, solution }: RowCompletedProps) => {
  const checkLetter = (letter: string, pos: number): BoxStatus => {
    // console.log(solution, letter);

    if (removeAccents(solution).includes(removeAccents(letter))) {
      if (removeAccents(solution[pos]) === removeAccents(letter))
        return "correct";
      else return "present";
    } else return "absent";
  };
  return (
    <div className={styles.row}>
      {Array.from(Array(5)).map((_, index) => (
        <Box
          key={index}
          value={word[index]}
          status={checkLetter(word[index], index)}
        />
      ))}
    </div>
  );
};

export default RowCompleted;
