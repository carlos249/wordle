import React from "react";
import styles from "./styles/row.module.scss";
import Box from "./Box";
interface RowCurrentProps {
  word: string;
}
const RowCurrent = ({ word }: RowCurrentProps) => {
  return (
    <div className={styles.row}>
      {word.split("").map((letter, index) => (
        <Box key={index} value={letter} status="edit" />
      ))}
      {Array.from(Array(5 - word.length)).map((letter, index) => (
        <Box key={index} value={letter} status="edit" />
      ))}
    </div>
  );
};

export default RowCurrent;
