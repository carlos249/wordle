import React, { useContext } from "react";
import "./styles/modalInstructions.scss";
import Box from "./Box";
import { SettingsContext } from "../context/SettingsContext";

interface ModalInstructionsProps {
  isShowModal: boolean;
  showModal: () => void;
  hiddenModal: () => void;
  chronometer: any;
}
export default function ModalInstructions({
  isShowModal,
  showModal,
  hiddenModal,
  chronometer,
}: ModalInstructionsProps) {
  const { settingsState } = useContext(SettingsContext);
  const firstArray = ["G", "A", "T", "O", "S"];
  const secondArray = ["V", "O", "C", "A", "L"];
  const threeArray = ["C", "A", "N", "T", "O"];
  return (
    <>
      {isShowModal ? (
        <>
          <div className="justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none">
            <div className="relative w-auto my-6 mx-auto max-w-3xl">
              {/*content*/}
              <div className="instructions border-black dark:border-modal-dark shadow-lg relative flex flex-col w-full bg-modal-light dark:bg-[#262B3C] outline-none focus:outline-none">
                {/*header*/}
                <div className="flex justify-center border-solid border-slate-200 rounded-t">
                  <h3 className="instructions-title self-center text-black dark:text-white">
                    Cómo jugar
                  </h3>
                </div>
                {/*body*/}
                <div className="relative flex-auto">
                  <p className="instructions-description text-black dark:text-white">
                    Adivina la palabra oculta en cinco intentos.
                  </p>
                  <br />
                  <p className="instructions-description text-black dark:text-white">
                    Cada intento debe ser una palabra válida de 5 letras.
                  </p>
                  <br />
                  <p className="instructions-description text-black dark:text-white">
                    Después de cada intento el color de las letras cambia para
                    mostrar qué tan cerca estás de acertar la palabra.
                  </p>
                  <h4 className="instructions-subtitle text-black dark:text-white">
                    Ejemplos
                  </h4>

                  <div className="flex ml-20 gap-x-4">
                    {firstArray.map((item, index) => (
                      <Box
                        key={item}
                        value={item}
                        status={
                          index === 0
                            ? "correct"
                            : settingsState.theme === "light"
                            ? "white"
                            : "dark"
                        }
                      />
                    ))}
                  </div>
                  <p className="instructions-description text-black dark:text-white mt-5 mb-9">
                    La letra <span>G</span> está en la palabra y en la posición
                    correcta.
                  </p>

                  <div className="flex ml-20 gap-x-4">
                    {secondArray.map((item, index) => (
                      <Box
                        key={item}
                        value={item}
                        status={
                          index === 2
                            ? "present"
                            : settingsState.theme === "light"
                            ? "white"
                            : "dark"
                        }
                      />
                    ))}
                  </div>

                  <p className="instructions-description instructions-description--m0 text-black dark:text-white mt-5 mb-9">
                    La letra <span>C</span> está en la palabra pero en la
                    posición incorrecta.
                  </p>

                  <div className="flex ml-20 gap-x-4">
                    {threeArray.map((item, index) => (
                      <Box
                        key={item}
                        value={item}
                        status={
                          index === 4
                            ? settingsState.theme === "light"
                              ? "absentLight"
                              : "absent"
                            : settingsState.theme === "light"
                            ? "white"
                            : "dark"
                        }
                      />
                    ))}
                  </div>
                  <p className="instructions-description text-black dark:text-white mt-5 mb-9">
                    La letra <span>O</span> no está en la palabra.
                  </p>
                  <p className="instructions-description text-black dark:text-white mt-11 mb-9">
                    Puede haber letras repetidas. Las pistas son independientes
                    para cada letra.
                  </p>
                  <p className="instructions-description text-center text-black dark:text-white mb-9">
                    ¡Una palabra nueva cada 5 minutos!
                  </p>
                  <button
                    className="instructions-button bg-[#6AAA64] self-center active:bg-emerald-500 uppercase  shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150"
                    type="button"
                    onClick={() => {
                      chronometer.start();
                      hiddenModal();
                    }}
                  >
                    !JUGAR¡
                  </button>
                </div>
              </div>
            </div>
          </div>
          <div className="opacity-25 fixed inset-0 z-40 bg-black"></div>
        </>
      ) : null}
    </>
  );
}
