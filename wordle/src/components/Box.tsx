import React from "react";
import styles from "./styles/box.module.scss";
import classNames from "classnames/bind";
import { BoxStatus } from "../configs/types";

const classes = classNames.bind(styles);

interface BoxProps {
  value: string;
  status: BoxStatus;
}
const Box = ({ value, status }: BoxProps) => {
  const boxStatus = classes({
    correct: status === "correct",
    present: status === "present",
    absent: status === "absent",
    absentLight: status === "absentLight",
    empty: status === "empty",
    edit: status === "edit",
    white: status === "white",
    dark: status === "dark",
  });
  return <div className={boxStatus}>{value}</div>;
};

export default Box;
