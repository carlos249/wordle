import React, { createContext, useContext, useState } from "react";
import HeaderTools from "../components/HeaderTools";
import { GameStatus } from "../configs/types";
import { SettingsContext } from "../context/SettingsContext";
import { useWindow } from "../hooks/useWindows";
import { keys } from "../services/keys";
import Keyboard from "../components/Keyboard";
import RowCompleted from "../components/RowCompleted";
import RowCurrent from "../components/RowCurrent";
import { RowEmpty } from "../components/RowEmpty";
import { useEffect } from "react";
import Request, { isValidWord } from "../services/request";
import { removeAccents } from "../services/request";
import ModalInstructions from "../components/ModalInstructions";
import { STORAGE } from "../configs/index";
import ModalStatistics from "../components/ModalStatistics";
import { useTimer } from "react-timer-hook";

const Wordle = () => {
  const { settingsState, addWin, addGames } = useContext(SettingsContext);
  const { getWord } = Request();
  const [wordSelect, setWordSelect] = useState<string>("");
  const [turn, setTurn] = useState<number>(1);
  const [currentWord, setCurrentWord] = useState<string>("");
  const [completeWords, setCompleteWords] = useState<string[]>([]);
  const [gameStatus, setGameStatus] = useState<GameStatus>(GameStatus.Playing);
  const [isShowModal, setIsShowModal] = useState<boolean>(false);
  const [isShowModalStatistics, setIsShowModalStatistics] =
    useState<boolean>(false);
  const time = new Date();
  time.setSeconds(time.getSeconds() + 300);

  useWindow("keydown", handleKeyDow);

  const isFirst = localStorage.getItem(STORAGE.isFirstTime);
  useEffect(() => {
    if (!isFirst) {
      setIsShowModal(true);
      localStorage.setItem(STORAGE.isFirstTime, "firstTime");
    }
    const word = getWord();
    // console.log("palabra", word);
    setWordSelect(word);
  }, []);

  const { seconds, minutes, isRunning, start, restart } = useTimer({
    expiryTimestamp: time,
    autoStart: !isFirst ? false : true,
    onExpire: () => {
      setCompleteWords([]);
      setTurn(1);
      const word = getWord();
      // console.log("palabra2", word);
      setWordSelect(word);
      setGameStatus(GameStatus.Playing);
      setCurrentWord("");
    },
  });

  const showModal = () => {
    setIsShowModal(true);
  };
  const hiddenModal = () => setIsShowModal(false);

  const showModalStatistics = () => {
    setIsShowModalStatistics(true);
  };
  const hiddenModalStatistics = () => setIsShowModalStatistics(false);

  function handleKeyDow(event: KeyboardEvent) {
    const letter = event.key.toUpperCase();
    onKeyPressed(letter);
  }

  function onKeyPressed(key: string) {
    if (gameStatus !== GameStatus.Playing) return;
    if (key === "BACKSPACE" && currentWord.length > 0) {
      onDelete();
      return;
    }
    if (key === "ENTER" && currentWord.length === 5 && turn <= 6) {
      onEnter();
      return;
    }
    if (currentWord.length >= 5) return;

    // INGRESAR LA LETRA AL ESTADO
    if (keys.includes(key)) {
      onInput(key);
      return;
    }
  }

  function onInput(letter: string) {
    const newWord = currentWord + letter;

    setCurrentWord(newWord);
  }
  function onDelete() {
    const newWord = currentWord.slice(0, -1);
    setCurrentWord(newWord);
  }
  function onEnter() {
    const wordRemoveAccent = removeAccents(currentWord);
    // console.log(currentWord, wordSelect);

    if (removeAccents(currentWord) === removeAccents(wordSelect)) {
      // GANO EL USUARIO
      setCompleteWords([...completeWords, currentWord]);

      setGameStatus(GameStatus.Won);
      addWin();
      addGames();
      showModalStatistics();
      return;
    }

    if (turn === 5) {
      // PERDIO EL USUARIO
      setCompleteWords([...completeWords, currentWord]);
      setGameStatus(GameStatus.Lost);
      addGames();
      showModalStatistics();
      return;
    }

    // VALIDAR SI EXISTE LA PALABRA
    if (wordRemoveAccent.length === 5 && !isValidWord(currentWord)) {
      alert("palabra no encontrada");
      return;
    }
    setCompleteWords([...completeWords, currentWord]);
    setTurn(turn + 1);
    setCurrentWord("");
  }
  return (
    <div className={`${settingsState.theme} `}>
      {isShowModal && (
        <ModalInstructions
          isShowModal={isShowModal}
          showModal={showModal}
          hiddenModal={hiddenModal}
          chronometer={{ start }}
        />
      )}
      <ModalStatistics
        isShowModal={isShowModalStatistics}
        type={turn < 5 ? "won" : "lost"}
        wordSelect={wordSelect}
        hiddenModal={hiddenModalStatistics}
        chronometer={{ minutes, seconds, isRunning, restart }}
      />
      <div className="dark:bg-wordle min-h-screen flex flex-col justify-evenly items-center">
        <HeaderTools
          showModalInstructions={showModal}
          showModalStatistics={showModalStatistics}
        />
        <div>
          {completeWords.map((word, index) => (
            <RowCompleted key={index} word={word} solution={wordSelect} />
          ))}
          {gameStatus === GameStatus.Playing ? (
            <RowCurrent word={currentWord} />
          ) : null}
          {Array.from(Array(5 - turn)).map((_, index) => (
            <RowEmpty key={index} />
          ))}
        </div>
        <Keyboard
          keys={keys}
          onKeyPressed={onKeyPressed}
          completeWords={completeWords}
          wordOfTheDay={wordSelect}
        />
      </div>
    </div>
  );
};

export default Wordle;
