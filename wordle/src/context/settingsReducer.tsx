import { SettingsState } from "./SettingsContext";

type SettingsAction =
  | { type: "darkMode"; payload: "dark" | "light" }
  | { type: "saveWord"; payload: string[] }
  | { type: "addGame"; payload: string }
  | { type: "addWin"; payload: string };

export const settingsReducer = (
  state: SettingsState,
  action: SettingsAction
): SettingsState => {
  switch (action.type) {
    case "darkMode":
      return {
        ...state,
        theme: action.payload,
      };
    case "saveWord":
      return {
        ...state,
        wordsRepet: action.payload,
      };

    case "addGame":
      return {
        ...state,
        games: action.payload,
      };
    case "addWin":
      return {
        ...state,
        win: action.payload,
      };

    default:
      return state;
  }
};
