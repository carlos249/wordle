import React, { createContext, useReducer } from "react";
import { settingsReducer } from "./settingsReducer";
import { STORAGE } from "../configs/index";

const themelocal = localStorage.getItem(STORAGE.theme);
const wordsRepetLocal = localStorage.getItem(STORAGE.wordsRepet);
const gamesLocal = localStorage.getItem(STORAGE.games);
const winLocal = localStorage.getItem(STORAGE.win);

export interface SettingsState {
  theme: "dark" | "light";
  wordsRepet: string[];
  games: string;
  win: string;
}

export const settingsInitialState: SettingsState = {
  theme: themelocal === null || themelocal === "light" ? "light" : "dark",
  wordsRepet:
    wordsRepetLocal !== null && Array.isArray(wordsRepetLocal)
      ? wordsRepetLocal
      : [],
  games: gamesLocal === null ? "0" : gamesLocal,
  win: winLocal === null ? "0" : winLocal,
};
export interface SettingsContextProps {
  settingsState: SettingsState;
  toggleTheme: (theme: "dark" | "light") => void;
  saveWordUse: (word: string[]) => void;
  addGames: () => void;
  addWin: () => void;
}
export const SettingsContext = createContext({} as SettingsContextProps);

export const SettingsProvider = ({ children }: any) => {
  const [settingsState, dispatch] = useReducer(
    settingsReducer,
    settingsInitialState
  );
  const toggleTheme = (theme: "dark" | "light") => {
    dispatch({ type: "darkMode", payload: theme });
    localStorage.setItem(STORAGE.theme, theme);
  };
  const saveWordUse = (arrayWord: string[]) => {
    dispatch({ type: "saveWord", payload: arrayWord });
    localStorage.setItem(STORAGE.wordsRepet, JSON.stringify(arrayWord));
  };

  const addGames = () => {
    const gamesLocal = localStorage.getItem(STORAGE.games);
    const countGames = String(Number(gamesLocal) + 1);
    dispatch({ type: "addGame", payload: countGames });
    localStorage.setItem(STORAGE.games, countGames);
  };
  const addWin = () => {
    const winLocal = localStorage.getItem(STORAGE.win);
    const countWin = String(Number(winLocal) + 1);
    dispatch({ type: "addWin", payload: countWin });
    localStorage.setItem(STORAGE.win, countWin);
  };
  return (
    <SettingsContext.Provider
      value={{ settingsState, toggleTheme, saveWordUse, addGames, addWin }}
    >
      {children}
    </SettingsContext.Provider>
  );
};
