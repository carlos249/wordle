import React, { useCallback, useContext, useMemo } from "react";
import { WORDS } from "./dictionary";
import { STORAGE } from "../configs/index";
import { SettingsContext } from "../context/SettingsContext";
function getWords() {
  return WORDS;
}

const Request = () => {
  const words = getWords();
  const wordsLength = useMemo((): string[] => {
    return words.filter((item) => item.length === 5);
  }, []);
  const { saveWordUse } = useContext(SettingsContext);

  function getWord() {
    let selectWord: string;
    let isFindWord: boolean = false;
    do {
      // selectWord = words[Math.floor(Math.random() * words.length)];
      selectWord = wordsLength[Math.floor(Math.random() * wordsLength.length)];
      selectWord = removeAccents(selectWord).toUpperCase();
      if (selectWord.length === 5) {
        isFindWord = true;
        const local = localStorage.getItem(STORAGE.wordsRepet);
        if (local !== null) {
          const arraySave = JSON.parse(local);
          const isRepeatWord = arraySave.find(
            (item: string) => item === selectWord
          );
          if (isRepeatWord) {
            isFindWord = true;
          } else {
            arraySave.push(selectWord);
            saveWordUse(arraySave);
          }
        } else {
          const arrayWords: string[] = [];
          arrayWords.push(selectWord);
          saveWordUse(arrayWords);
        }
      }
    } while (!isFindWord);
    return selectWord;
  }
  return { getWord };
};

export const removeAccents = (str: string): string => {
  return str.normalize("NFD").replace(/\p{Diacritic}/gu, "");
};
export function isValidWord(word: string) {
  const words = getWords();
  let isInclude = false;
  for (let index = 0; index < words.length && !isInclude; index++) {
    const element = removeAccents(words[index]);
    if (element.toLowerCase() === removeAccents(word.toLowerCase()))
      isInclude = true;
  }
  return isInclude;
}
export default Request;
