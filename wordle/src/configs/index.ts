export const STORAGE = {
  isFirstTime: "isFirstTime",
  theme: "THEME",
  wordsRepet: "WORDSREPEAT",
  games: "GAMES",
  win: "WIN",
};
