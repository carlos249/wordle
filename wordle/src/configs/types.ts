export type BoxStatus =
  | "absent"
  | "absentLight"
  | "present"
  | "correct"
  | "empty"
  | "edit"
  | "white"
  | "dark";

export const enum GameStatus {
  Playing,
  Won,
  Lost,
}
